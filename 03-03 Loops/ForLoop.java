public class ForLoop{
	// Using label in for loop

	public static void main(String[] args) {

		// int i=0;
		// for( ; ; ){
		// 	if(i<args.length){
		// 		i++;
		// 		System.out.println("Current number is "+i);
		// 	}else{
		// 		break;
		// 	}
		// }


	// example of using continue and break in the loop
		for(int i = 1; i<=60; i++){
			if(i == 29){
				System.out.println("Only the leap year has 29-th day in February");
				continue;
			}

			if(i == 30){
				System.out.println("Most months of the year has 30 days");
				continue;
			}

			System.out.println("Current day of this month: "+i);

			if( i == 31){
				break;
			}
		}

	}
}