public class LabelLoops{
	public static void main(String[] args) {
		int x = 0;
		int j = 5;
		myLableA:
			for( ; x<10; x++){
				System.out.println("x:j" + x + " : " + j);
				if(x > j) break myLableA;
			}
		System.out.println("This block will be printed after break myLabelA");
	}
}