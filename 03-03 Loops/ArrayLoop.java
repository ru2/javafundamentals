public class ArrayLoop{
	public static void main(String[] args) {

		// initializing array using for loop
		int[][] myArray = new int[20][10];
		for(int i=0; i<20; i++){
			for(int j=0; j<10; j++){
				myArray[i][j] = i*j;
				System.out.print(myArray[i][j]+ " | ");

			}
		}

		// printing array elements using loops
		for(int x=0; x<10; x++){
			System.out.println(myArray[3][x]+ " | ");

		}

		// advanced for loop  
		// used for  printing elements of an array
		for(int i : myArray[4]){
			System.out.println(i);
		}
	}
}