public class WhileLoop {
	public static void main(String[] args) {
		boolean myBoolean = true;
		int j = 0;

		// while loop uses true-false condition for starting work
		while(myBoolean != false){
			System.out.println("Current number is " + j);
			j++;
			myBoolean = j >= 50? false : true;
		}


		// most common with while loop
		// but it will iterate at least once
		do{
			System.out.println("Current number is " + j);
			j++;
		}while(j<0);
	}
}