public class Squirrel extends Animal implements Fleshbird, Mammals{
    public String name = "Flying Squirrel";

    @Override
    public void getName() {
        System.out.println("It's a forest animal - " + name);
    }

    public void fly() {
        System.out.println("Can fly at distance of 90 m");
    }

    public void run() {
        System.out.println("Very quick get nuts");
    }
}
