import java.util.ArrayList;
import java.util.List;

public class App {
    public static void main(String[] args) {

        Squirrel squirrel = new Squirrel();
        squirrel.getName();
        squirrel.fly();
        squirrel.run();
        squirrel.eat();

        Lion lion = new Lion();
        lion.getName();
        lion.run();

        Eagle eagle = new Eagle();
        eagle.getName();
        eagle.fly();

        Squirrel squirrel1 = new Squirrel();
        Lion lion1 = new Lion();
        Eagle eagle1 = new Eagle();

//        Birds birds = new Squirrel();
//        System.out.println(birds.maxHeight);


        List<String> animalNames = new ArrayList();
        animalNames.add(squirrel.name);
        animalNames.add(lion.name);
        animalNames.add(eagle.name);
        animalNames.add(squirrel1.name);
        animalNames.add(lion1.name);
        animalNames.add(squirrel1.name);
        animalNames.add(eagle1.name);

//        for(String single : animalNames){
//            System.out.println(single.toUpperCase());
//        }

        animalNames.replaceAll(n->n.toUpperCase());
        animalNames.forEach(n-> System.out.println(n));
    }
}
