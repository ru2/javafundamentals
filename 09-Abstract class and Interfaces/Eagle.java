public class Eagle extends Animal implements Birds {
    public String name = "Eagle";

    @Override
    public void getName() {
        System.out.println("It's a mountains animal - " + name);
    }

    @Override
    public void fly() {
        System.out.println("Max height 9 km");
    }
}