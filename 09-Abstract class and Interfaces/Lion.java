public class Lion extends Animal implements Mammals{
    public String name = "Lion";

    @Override
    public void getName() {
        System.out.println("It's a jungle animal - " + name);
    }

    @Override
    public void run(){
        System.out.println("Max speed 80 km/h");
    }

}
