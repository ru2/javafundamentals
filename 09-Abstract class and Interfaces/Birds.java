public interface Birds {
    int maxHeight = 12;
    void fly();

    static void hunt(){
        System.out.println("Can hunt");
    }

    default void eat(){
        System.out.println("Can eat");
    }
}
