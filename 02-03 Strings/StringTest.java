public class StringTest{
	public static void main(String[] args) {
		char[] myCharArray = {'a', 'b', 'c'};
		String myStringFromChars = new String (myCharArray);

		String myString1 = "ScripTehInfo ";
		String myString2 = "School";
		String myString6 = "School";
		String myString3 = new String("ScripTehInfo School");
		String myString4 = new String ("ScripTehInfo ");
		String myString5 = new String("School");
		
		System.out.println(myString2 == myString5);
		System.out.println(myString2 == myString6); 		// compare references for each object
		System.out.println(myString2.equals(myString5));	// compare value inside objects
		System.out.println(myString3.equals(myString1 + myString2));
		System.out.println(myString3 == (myString1 + myString2));
	}	
}