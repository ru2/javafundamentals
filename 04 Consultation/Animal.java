public class Animal{
	private String species; 
	private int numOfLegs;
	private double weight;
	private double length;

	public Animal(){
		this.species = "Dog";
		this.numOfLegs = 4;
		this.weight = 20;
		this.length = 80;
	}

	public Animal(double weight, double length){
		this.species = "Fish";
		this.numOfLegs = 0;
		this.weight = weight;
		this.length = length;
	}

	public void setSpecies(String newSpecies){
		this.species = newSpecies;
	}

	public String getSpecies(){
		return this.species;
	}

	/*
	public Animal(String species, double length){
		this.species = species;
		this.numOfLegs = 2;
		this.weight = weight;
		this.length = length;
	}

	public Animal(double length, String name){
		this.species = name;
		this.numOfLegs = 0;
		this.weight = weight;
		this.length = length;
	}
	*/

	/* Rules for oveload constructors:
		- number of arguments - different
		- different types of arguments
		- different order of arguments
	*/

	public static void main(String[] args) {
		Animal dog1 = new Animal();
		Animal dog2 = new Animal();
		Animal dog3 = new Animal();

		Animal fish1 = new Animal(4.0, 50.0);
		Animal fish2 = new Animal(5.0, 80.0);
		Animal fish3 = new Animal(1.0, 35.0);

		Animal [] zoo = new Animal[] {dog1, dog2, dog3, fish1, fish2, fish3};

		dog1.setSpecies("Old Big Dog");

		System.out.println();
		for(Animal currentAnimal : zoo){
			System.out.println("The name is " + currentAnimal.species
								+ ", number of legs is " + currentAnimal.numOfLegs 
								+ ", weight is " + currentAnimal.weight
								+ ", length is " + currentAnimal.length
				);	
		}
		
	}
}