public class WrappersTest{

	boolean myBoolean;
	byte myByte;
	short myShort;
	char myChar;
	int myInt; 
	long myLong;
	float myFloat;
	double myDouble;

	Boolean myBooleanW = false;
	Byte myByteW = myByte; // autoboxing;
	Short myShortW = 200;
	Character myCharacter = 'e';
	Integer myInteger = 365;
	int myInt1 = Integer.valueOf(myInteger);  // unboxing
	Long myLongW = 60000009999998888l;
	Float myFloatW = 6.08f;
	Double myDoubleW = 23.99;

}