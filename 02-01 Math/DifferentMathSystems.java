public class DifferentMathSystems{
	public static void main(String[] args) {
		int myDec = 10;
		int myBinary = 0b01010;
		int myOct = 010; // 0 7 - 7, 8 - 10, 9 - 11, 10 - 12
		int myXed = 0x0909A; // 0x ABCDEF

		System.out.println(myXed);
	}
}