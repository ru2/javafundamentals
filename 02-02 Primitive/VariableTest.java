public class VariableTest{
	
	public static void main(String[] args) {
		char a = 'a';		// line 1
		int c = 'c'; 		// line 2
		char b = 200 ;		// line 3
		//a = c; 				// line 4
		c = b; 				// line 5
	}
}