// Animal class HAS-A subclass of Sheep (HAS-A relationship)

public class Animal {
    private String color = "white";

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    protected String type = "mammal";

    public Animal(){
        System.out.println("Hello I'm Animal");
    }

    public void eat() {
        System.out.println("I can eat");
    }

    public void sleep() {
        System.out.println("I can sleep");
    }
}