// Sheep class IS-A subclass of Animal (IS-A relationship)

public class Sheep extends Animal {
    public String type = "Mammal";

    public void displayInfo() {
        System.out.println("I have " + getColor() + " color ");
        System.out.println("I am " + this.type);
    }

    public void baa() {
        System.out.println("I can baa");
    }

    @Override //Polymorphism
    public void eat() {
        System.out.println("I'm sheep and I eat grass");
    }
}