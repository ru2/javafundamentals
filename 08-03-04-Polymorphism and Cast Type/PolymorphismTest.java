public class PolymorphismTest {
    public static void main(String[] args) {
        System.out.println("\n Original Parent class");
        Animal animal = new Animal();
        animal.eat();
        animal.sleep();
        System.out.println(animal.getColor());
//        ((Sheep) animal).eat(); Exception in Runtime

        System.out.println("\n Inheritance");
        Sheep sheep = new Sheep();
        sheep.setColor("black");
        sheep.sleep();
        sheep.eat();
        sheep.displayInfo();

        System.out.println("\n Polymorphism");
        Animal animal1 = new Sheep();
        System.out.println(animal1.getColor());
        System.out.println(animal1.type);
        animal1.eat();
        animal1.sleep();
        ((Sheep)animal1).baa();
        ((Animal)sheep).eat();

        System.out.println("\n Cast errors");
//        Sheep sheep3 = new Animal(); Compile Error
        Sheep sheep1 = new Sheep();
        Animal animal2 = sheep1;
        ((Sheep)animal2).baa();
    }
}
