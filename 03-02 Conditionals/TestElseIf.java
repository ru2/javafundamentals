class TestElseIf{
	public static void main(String[] args) {
		// int i = 20;
		// if(i>30){
		// 	System.out.println("Var i bigger that 30");
		// }else if(10<i && i<30){
		// 	System.out.println("Var i bigger that 10 and smaller that 30");
		// }else{
		// 	System.out.println("Neither one is working");
		// }

		String message = "Condition for if-else block";
		if(message.length() > 10)
			System.out.println(message + " is true ");
			// System.out.println("This is second line in if block"); // This is problem line
		else 
			System.out.println(message + " is false ");
			System.out.println("This block will be printed anyway");
	}
}