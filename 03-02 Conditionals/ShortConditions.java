public class ShortConditions{
	public static void main(String[] args) {
		int a = 5;
		int b = 6;

		// if(b > 0){
		// 	a = 35;
		// }else{
		// 	a = 10;
		// }

		a = b>0 ? a++ + 35 : 10 - --b;
		System.out.println(a);
	}
}