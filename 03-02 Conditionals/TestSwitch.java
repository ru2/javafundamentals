public class TestSwitch{
	public static void main(String[] args) {
		// Switch works with next types: byte, char, short, int, String and their Wrappers
		// Byte, Short, Character, Integer, Enum

		// This is prohibited in switch
			// long, float, double, boolean and their Wrappers
			// null case 
			// repeat condition for case

		String mathOperation = args[0];
		switch(mathOperation){
			case"+": 
				System.out.println("This is adding operation");
				break;
			// case"+":
			// 	System.out.println("This is adding operation");
			// 	break;
			case "-": 
				System.out.println("This is substracting operation");
				break;
			case"*": 
				System.out.println("This is multipling operation");
				break;
			case"/": 
				System.out.println("This is dividing operation");
				break;
			default:
				System.out.println("Please insert the correct math operation");
		}

		// switch(mathOperation){
		// 	mathOperation = "-";
		// }
	}
}