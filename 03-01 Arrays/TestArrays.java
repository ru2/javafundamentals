public class TestArrays{
	public static void main(String[] args) {
		int[] myArray = new int[10];
		myArray[0] = 2020;
		myArray[1] = 2021;
		System.out.println(myArray[0]);

		// Correct way to create arrays
		String[][] myStringArray = new String[10][3];
		System.out.println(myStringArray[5][2]);

		int[] mySecondArray = {1, 2, 4, 5, 8, 3, 9, 4}; 

		// Wrong way
		// float[] myFloatArray = new [][];
		// float[] myFloatArray = new [];
		// float[] myFloatArray = new float[];

		// float myFloat[3][5] = new float[3][5];

		int[][] myComplexArray = new int[3][3];
		myComplexArray[2] = new int[120];
		System.out.println(myComplexArray[0][2]);
		System.out.println(myComplexArray[1][2]);
		System.out.println(myComplexArray[2][100]);


	}
}