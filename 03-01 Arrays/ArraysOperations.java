import java.util.Arrays;

class ArraysOperations{
	public static void main(String[] args) {
		String[] myClass = {"Oleg", "Ruslan", "Yura", "Oleg", "Sam", "Edward"};
		System.out.println("Here is our array " + Arrays.toString(myClass));
		System.out.println("Length of my array " + myClass.length);
		Arrays.sort(myClass);
		System.out.println("Sorting my array " + Arrays.toString(myClass));
	}
}