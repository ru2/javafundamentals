public class MyLambdaClass implements LambdaInterface {
    @Override
    public String printOut(String myString){
        return null;
    }

    public static void main(String[] args) {
        LambdaInterface myVariable = m->"Hello "+ m;
        System.out.println(myVariable.printOut("Edward"));

        LambdaInterface myName = n->n.toUpperCase();
        System.out.println(myName.printOut("Edward Rudoi"));
    }

    // if you have more thatn one argument (n, m)-> you must use ()
    // if you have return in lambda you must use { }, and ; inside { ;};
    // return, (;) and { } - goes together
}
