import java.util.ArrayList;
import java.util.Iterator;

public class TestArrayList {
    public static void main(String[] args) {
        ArrayList<String> persons = new ArrayList<>();
        persons.add("John");
        persons.add("Peter");
        persons.add("James");
        persons.add("Andrew");
        persons.add("James");

        System.out.println(persons);

        System.out.println(persons.remove((int) new Integer(2)));
        System.out.println(persons);

        System.out.println(persons.get(2));

        persons.set(0, null);   // difference
        persons.add(0, "Jude");

        System.out.println(persons);

        System.out.println(persons.size());
        System.out.println(persons.contains("James"));

        System.out.println(persons.get(1));

        System.out.println("----------------------------");


        Iterator personsIterator = persons.iterator();
        while (personsIterator.hasNext()){
            System.out.println(personsIterator.next());
        }
    }
}