import java.util.*;

public class TestMap {
    public static void main(String[] args) {
        Map<String, Integer> apostoleAge = new HashMap<>();
        apostoleAge.put("John", 20);
        apostoleAge.put("Peter", 50);
        apostoleAge.put("Andrew", 35);
        apostoleAge.put("Andrew", 12);

        System.out.println(apostoleAge);

        System.out.println(apostoleAge.get(3)); // output null
        System.out.println(apostoleAge.get("John")); // output 20

                                                // Set
        Iterator<String> myIteratorKeys = apostoleAge.keySet().iterator();
//      Iterator<Integer> myIteratorValues = apostoleAge.values().iterator();

        while (myIteratorKeys.hasNext()){
            String myKey = myIteratorKeys.next();
            Integer myValue = apostoleAge.get(myKey);
            System.out.println(myKey + " - " + myValue);
        }

        // Map.Entry<> - homework
        // Collections Queue (peek, push)

    }
}
