public class OverloadTest{
	public String name; 
	public int age;
	
	public void showPersonInfo(String name){
		System.out.println("Name is " + name + " and age is " + this.age);
	}

	public void showPersonInfo(int age){
		System.out.println("Name is " + this.name + " and age is " + age);
	}


	public static void main(String[] args) {
		OverloadTest firstObject = new OverloadTest();
		firstObject.showPersonInfo(20);
	}
}