import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

public class TryReadFlie {
    public static void main(String[] args) {
        Path source = Paths.get("/Users/ed/Dropbox/Java Teaching/Source.txt");

        try {
            readFile(source);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static void readFile(Path source) throws IOException { // 4. throws to caller method
        BufferedReader bufferedReader = null;

        try {                                                    // 1. surround with try catch
            bufferedReader = Files.newBufferedReader(source);
            System.out.println(bufferedReader.readLine());
            throw new MyOwnException();                         //  5. throw new Exception
        } catch (MyOwnException e) {                                // 2. try with catch (multiple same)
            e.printStackTrace();
        } finally {                                                 // 3. try with finally
            Objects.requireNonNull(bufferedReader).close();
        }
    }

    static class MyOwnException extends Exception{

    }
}
