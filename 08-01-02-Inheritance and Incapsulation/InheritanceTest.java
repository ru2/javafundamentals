public class InheritanceTest {
    public static void main(String[] args) {
        Sheep sheep = new Sheep();
        sheep.eat();
        sheep.sleep();
        sheep.baa();

        sheep.type = "mammal";
    }
}
