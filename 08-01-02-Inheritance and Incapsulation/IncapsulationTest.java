public class IncapsulationTest {
    public static void main(String[] args) {
        Sheep sheep2 = new Sheep();
        sheep2.setColor("white");
        sheep2.type = "mammal";

        sheep2.displayInfo();
    }
}
