public class Animal {
    // 4 type of access modifiers
    // public - for all
    // protected - only for class and subclasses (child_class)
    // default - doesn't have keyword - empty (more strict that protected) - package-private
    // private - only for this class


    protected String type;
    String name;

    private String color;
    public String getColor() {
        return color;
    }
    public void setColor(String color) {
        this.color = color;
    }


    public void eat() {
        System.out.println("I can eat");
    }

    public void sleep() {
        System.out.println("I can sleep");
    }
}

