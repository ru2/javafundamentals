public class InitializationOrder{
	
	public String nonStaticVar1 = nonStaticMethod("Non StaticVar 1");
	public String nonStaticVar2 = nonStaticMethod("Non StaticVar 2");

	public String nonStaticMethod(String someString){
		System.out.println("Outupt from Non-Static " + someString);
		return someString;
	}

	{
		System.out.println("Output from Non-static initialization block");
	}

	public static String staticVar1 = staticMethod("StaticVar 1");
	public static String staticVar2 = staticMethod("StaticVar 2");
	
	public static String staticMethod(String someString){
		System.out.println("Outupt from static " + someString);
		return someString;
	}

	static {
		System.out.println("Output from static initialization block");
	}

	public InitializationOrder(){
		System.out.println("Output from constructor");
	}

	public static void main(String[] args) {
		InitializationOrder myObject = new InitializationOrder();
	}

	// static - primary
		// in order of appearance
	// non-static after static
		// in order of appearance
	// constructor at the end

}