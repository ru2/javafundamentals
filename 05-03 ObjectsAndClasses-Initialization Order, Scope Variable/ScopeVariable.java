public class ScopeVariable{
	public String scope = "Global scope";
	public int count = 10;

	public ScopeVariable(String scope){
		scope = scope;
		System.out.println(scope);
	}

	public void changeLocalVariable(int count){
		count = 15;
		System.out.println(count);
			if(true){
				for(int j = 0; j<15; j++)
					System.out.println("Hello");
					// System.out.println(j); 		will not compile
			}
	}

	public void ifElseVariableScope(){
		int count = 20;
		int j = 1;
		// int rate = 0;

		if(count > j){
			int rate = 19;
			System.out.println("YOu need to buy more USD" + rate);
		}else{
			int rate = 21;
			System.out.println("It's enought" + rate);	
		}
	}

	public static void main(String[] args) {
		ScopeVariable firstObject = new ScopeVariable("From main");
		System.out.println(firstObject.scope);

		firstObject.changeLocalVariable(33);
	}
}