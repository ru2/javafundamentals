public class StaticTest{
	public String name;
	public static int counter;

	public StaticTest(String personName){
		this.name = personName;
		counter++;
	}

	public void showObjectInfo(){
		System.out.println(this.name);
		System.out.println(counter);
	}

	public static void showFromStatic(){
		// System.out.println(this.name);			// compile error
		System.out.println("Show counter from static "+ counter);
	}

	public static void main(String[] args) {
		// StaticTest firstPerson = new StaticTest("Andrew");
		// StaticTest secondPerson = new StaticTest("Peter");
		// secondPerson.counter = 200;
		// firstPerson.showObjectInfo();
		// secondPerson.showObjectInfo();

		StaticTest.showFromStatic();
		
		// System.out.println(firstPerson.name + " " + firstPerson.counter); 	//??
		// System.out.println(secondPerson.name + " " + secondPerson.counter); 	//??
	}
}