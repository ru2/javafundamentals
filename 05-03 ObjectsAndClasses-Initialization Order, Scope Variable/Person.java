public class Person {
    public static String specie = "Om";
    public String name;
    public int age;
    public String occupation;

    public Person() {
        age = 23;
        name = "John";
        occupation = "student";
    }

    public Person(String firstName) {
        this();
        this.name = firstName;
    }

    public Person(int age, String name, String occupation){
        this.age = age;
        this.name = name;
        this.occupation = occupation;
    }

    public static void main(String[] args) {s
        Person person1 = new Person();
        person1.specie = "Om bun";
        System.out.println(person1);

    }

    public void setName(String firstName) {
        this.name = firstName;
    }
}